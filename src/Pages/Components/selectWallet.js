import React, { Component } from 'react';
import { user, db } from '../../API/user';
import styles from './profileModal.module.css';

import ftx_icon from '../../assets/images/ftx-icon.png';
import cryptoPlease_icon from '../../assets/images/cryptoplease_icon.jpeg';
import phantom_icon from '../../assets/images/phantom_icon.png';

const roleOptions = [ { label: 'Landlord', value: 'landlord' }, { label: 'Tenant', value: 'tenant' } ];

const walletOptions = [
	{ label: 'Phantom', value: 'phantom' },
	{ label: 'Crypto Please', value: 'cryptoplease' },
	{ label: 'FTX', value: 'ftx' }
];

class SelectWallet extends Component {
	constructor(props) {
		super(props);
		console.log(props.wallet);
		this.state = {
			firstName: props.profile.firstName,
			lastName: props.profile.lastName,
			role: props.profile.role,
			wallet: props.wallet
		};
	}

	logout = async () => {
		await user.leave();
		window.location.reload();
	};

	updateUser = async () => {
		const { firstName, lastName, role, wallet } = this.state;
		console.log(this.state);
		if (role != this.props.profile.role) {
			await user.get('profile').put({ firstName: firstName, lastName: lastName, role: role }).then();
		} else {
			await user.get('profile').put({ firstName: firstName, lastName: lastName }).then();
		}
		if (wallet != this.props.wallet) {
			await user.get('wallet').put({ type: wallet.type, address: wallet.address }).then();
		}
		//await user.get('wallet').then()
		window.open('/', '_self');
		this.props.close();
	};

	render() {
		const { firstName, lastName, role, wallet } = this.state;

		const initials = `${firstName && firstName != '' ? firstName.toUpperCase().charAt(0) : ''}${lastName &&
		lastName != ''
			? lastName.toUpperCase().charAt(0)
			: ''}`;
		return (
			<div className="modalContainer">
				<div className={[ 'modalBox', styles.modalBox ].join(' ')}>
					<div className={styles.modalContent}>
						<h1>Add Wallet</h1>

						<div>
							<p>Please select a wallet to connect. </p>
						</div>

						<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
							<div className={styles.logoContainerWallet}>
								<div className={styles.walletTile}>
									<img
										src={phantom_icon}
										onClick={() => {
											console.log('phantom');
											this.setState({ wallet: { ...this.state.wallet, type: 'phantom' } });
										}}
										className={wallet.type == 'phantom' && styles.walletSelected}
									/>
									<div className={styles.walletLabel}>
										<p>Phantom</p>
									</div>
								</div>
								<div className={styles.walletTile}>
									<img
										src={cryptoPlease_icon}
										className={wallet.type == 'cryptoplease' && styles.walletSelected}
										onClick={() => {
											console.log('Crypto Please');
											this.setState({ wallet: { ...this.state.wallet, type: 'cryptoplease' } });
										}}
									/>
									<div className={styles.walletLabel}>
										<p>Crypto Please</p>
									</div>
								</div>
								<div className={styles.walletTile}>
									<img
										src={ftx_icon}
										className={wallet.type == 'ftx' && styles.walletSelected}
										onClick={() => {
											console.log('FTX');
											this.setState({ wallet: { ...this.state.wallet, type: 'ftx' } });
										}}
									/>
									<div className={styles.walletLabel}>
										<p>FTX</p>
									</div>
								</div>
							</div>
						</div>
						{wallet &&
						wallet.type !== '' && (
							<div>
								Wallet:
								<input
									className={styles.cardInput}
									placeholder="Wallet Address"
									value={wallet.address}
									onChange={(e) =>
										this.setState({ wallet: { ...this.state.wallet, address: e.target.value } })}
								/>
							</div>
						)}
						<button className={styles.primaryButton} onClick={this.updateUser}>
							SAVE
						</button>
					</div>
				</div>
			</div>
		);
	}
}

export default SelectWallet;
