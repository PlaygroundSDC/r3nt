import React, { Component } from 'react';
import { user, db } from '../../API/user';
import styles from './propertyDetailsModal.module.css';
import Loader from './loader';

const states = [
	{
		label: 'Alabama',
		value: 'Alabama'
	},
	{
		label: 'Alaska',
		value: 'Alaska'
	},
	{
		label: 'American Samoa',
		value: 'American Samoa'
	},
	{
		label: 'Arizona',
		value: 'Arizona'
	},
	{
		label: 'Arkansas',
		value: 'Arkansas'
	},
	{
		label: 'California',
		value: 'California'
	},
	{
		label: 'Colorado',
		value: 'Colorado'
	},
	{
		label: 'Connecticut',
		value: 'Connecticut'
	},
	{
		label: 'Delaware',
		value: 'Delaware'
	},
	{
		label: 'District Of Columbia',
		value: 'District Of Columbia'
	},
	{
		label: 'Federated States Of Micronesia',
		value: 'Federated States Of Micronesia'
	},
	{
		label: 'Florida',
		value: 'Florida'
	},
	{
		label: 'Georgia',
		value: 'Georgia'
	},
	{
		label: 'Guam',
		value: 'Guam'
	},
	{
		label: 'Hawaii',
		value: 'Hawaii'
	},
	{
		label: 'Idaho',
		value: 'Idaho'
	},
	{
		label: 'Illinois',
		value: 'Illinois'
	},
	{
		label: 'Indiana',
		value: 'Indiana'
	},
	{
		label: 'Iowa',
		value: 'Iowa'
	},
	{
		label: 'Kansas',
		value: 'Kansas'
	},
	{
		label: 'Kentucky',
		value: 'Kentucky'
	},
	{
		label: 'Louisiana',
		value: 'Louisiana'
	},
	{
		label: 'Maine',
		value: 'Maine'
	},
	{
		label: 'Marshall Islands',
		value: 'Marshall Islands'
	},
	{
		label: 'Maryland',
		value: 'Maryland'
	},
	{
		label: 'Massachusetts',
		value: 'Massachusetts'
	},
	{
		label: 'Michigan',
		value: 'Michigan'
	},
	{
		label: 'Minnesota',
		value: 'Minnesota'
	},
	{
		label: 'Mississippi',
		value: 'Mississippi'
	},
	{
		label: 'Missouri',
		value: 'Missouri'
	},
	{
		label: 'Montana',
		value: 'Montana'
	},
	{
		label: 'Nebraska',
		value: 'Nebraska'
	},
	{
		label: 'Nevada',
		value: 'Nevada'
	},
	{
		label: 'New Hampshire',
		value: 'New Hampshire'
	},
	{
		label: 'New Jersey',
		value: 'New Jersey'
	},
	{
		label: 'New Mexico',
		value: 'New Mexico'
	},
	{
		label: 'New York',
		value: 'New York'
	},
	{
		label: 'North Carolina',
		value: 'North Carolina'
	},
	{
		label: 'North Dakota',
		value: 'North Dakota'
	},
	{
		label: 'Northern Mariana Islands',
		value: 'Northern Mariana Islands'
	},
	{
		label: 'Ohio',
		value: 'Ohio'
	},
	{
		label: 'Oklahoma',
		value: 'Oklahoma'
	},
	{
		label: 'Oregon',
		value: 'Oregon'
	},
	{
		label: 'Palau',
		value: 'Palau'
	},
	{
		label: 'Pennsylvania',
		value: 'Pennsylvania'
	},
	{
		label: 'Puerto Rico',
		value: 'Puerto Rico'
	},
	{
		label: 'Rhode Island',
		value: 'Rhode Island'
	},
	{
		label: 'South Carolina',
		value: 'South Carolina'
	},
	{
		label: 'South Dakota',
		value: 'South Dakota'
	},
	{
		label: 'Tennessee',
		value: 'Tennessee'
	},
	{
		label: 'Texas',
		value: 'Texas'
	},
	{
		label: 'Utah',
		value: 'Utah'
	},
	{
		label: 'Vermont',
		value: 'Vermont'
	},
	{
		label: 'Virgin Islands',
		value: 'Virgin Islands'
	},
	{
		label: 'Virginia',
		value: 'Virginia'
	},
	{
		label: 'Washington',
		value: 'Washington'
	},
	{
		label: 'West Virginia',
		value: 'West Virginia'
	},
	{
		label: 'Wisconsin',
		value: 'Wisconsin'
	},
	{
		label: 'Wyoming',
		value: 'Wyoming'
	}
];

const rentCycles = [ 'monthly', 'bimonthly' ];

const types = [ 'Room', 'Apartment', 'House', 'Condo', 'Boat', 'Shed', 'Other' ];

class PropertyDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: props.property.name,
			type: props.property.type,
			otherType: props.property.otherType,
			address1: props.property.address1,
			address2: props.property.address2,
			city: props.property.city,
			state: props.property.state,
			zipCode: props.property.zipCode,
			squareFeet: props.property.squareFeet,
			roomsAvailable: props.property.roomsAvailable,
			bathroomsAvailable: props.property.bathroomsAvailable,
			rentCycle: props.property.rentCycle,
			rentDueDates: props.property.rentDueDates,
			rentAmount: props.property.rentAmount,
			tenants: {},
			creating: false
		};
	}

	componentDidMount() {
		const { property } = this.props;
		user.get('rentalProperties').get(property.id).get('tenants').map().once(async (tenant, id) => {
			const tenantProfile = await db.user(tenant.key).get('profile').then();
			const tenantEmail = await db.user(tenant.key).get('alias').then();
			if (tenantProfile) {
				const tenants = this.state.tenants;
				tenants[tenant.key] = { email: tenantEmail, profile: tenantProfile };
				this.setState({ tenants: tenants });
			}
		});
	}

	saveProperty = async () => {
		const {
			name,
			address1,
			address2,
			city,
			state,
			zipCode,
			squareFeet,
			type,
			otherType,
			roomsAvailable,
			bathroomsAvailable,
			description,
			rentCycle,
			rentDueDates,
			rentAmount
		} = this.state;
		let errors = {};
		this.setState({ saving: true });
		const rentDueDatesArray = rentDueDates.split(',');
		switch (rentCycle) {
			case 'monthly':
				errors['rentDueDates'] =
					rentDueDatesArray.length == 1 &&
					parseInt(rentDueDatesArray[0]) != NaN &&
					parseInt(rentDueDatesArray[0]) < 32 &&
					parseInt(rentDueDatesArray[0]) > 0
						? null
						: 'Please enter a valid date';
				break;
			case 'bimonthly':
				errors['rentDueDates'] =
					rentDueDatesArray.length == 2 &&
					parseInt(rentDueDatesArray[0]) != NaN &&
					parseInt(rentDueDatesArray[0]) < 32 &&
					parseInt(rentDueDatesArray[0]) > 0 &&
					parseInt(rentDueDatesArray[1]) != NaN &&
					parseInt(rentDueDatesArray[1]) < 32 &&
					parseInt(rentDueDatesArray[1]) > 0 &&
					parseInt(rentDueDatesArray[0]) < parseInt(rentDueDatesArray[1])
						? null
						: 'Please enter 2 valid dates seperated by a comma';
				break;
			default:
				errors['rentCycle'] = 'Not valid';
		}
		errors['name'] = name == '' ? 'Name required' : null;
		errors['type'] = type == '' ? 'Type required' : null;
		errors['address1'] = address1 == '' ? 'Address 1 required' : null;
		errors['city'] = city == '' ? 'City required' : null;
		errors['state'] = state == '' ? 'State required' : null;
		errors['zipCode'] = zipCode == '' ? 'Zip Code required' : null;
		errors['type'] = type == '' ? 'Type required' : null;
		if (type == 'Other') {
			errors['otherType'] = otherType == '' ? 'Type required' : null;
		}
		if (!Object.values(errors).some((e) => !!e)) {
			const property = {
				name: name,
				type: type,
				otherType: otherType,
				address1: address1,
				address2: address2,
				city: city,
				state: state,
				zipCode: zipCode,
				squareFeet: squareFeet,
				roomsAvailable: roomsAvailable,
				bathroomsAvailable: bathroomsAvailable,
				description: description,
				rentCycle: rentCycle,
				rentDueDates: rentDueDates,
				rentAmount: rentAmount
			};
			const saved = await user.get('rentalProperties').get(this.props.propertyKey).put({ ...property });
			this.setState({ errors: errors, saving: false, success: true });
		} else {
			this.setState({ errors: errors, saving: false, success: false });
		}
	};

	render() {
		const {
			name,
			address1,
			address2,
			city,
			state,
			zipCode,
			squareFeet,
			type,
			otherType,
			roomsAvailable,
			bathroomsAvailable,
			description,
			creating,
			rentCycle,
			rentDueDates,
			rentAmount,
			tenants
		} = this.state;
		const tenantKeys = Object.keys(tenants);
		return (
			<div className="modalContainer">
				<div className={[ 'modalBox', styles.modalBox ].join(' ')}>
					<div className={styles.modalContent}>
						<h1>Property Details</h1>
						<p>Update information and see you tenant status</p>
						<p>
							<b>LOCATION</b>
						</p>
						<input
							type="text"
							className={styles.cardInput}
							placeholder="Name"
							value={name}
							onChange={(e) => this.setState({ name: e.target.value })}
						/>
						<input
							type="text"
							className={styles.cardInput}
							placeholder="Address 1"
							value={address1}
							onChange={(e) => this.setState({ address1: e.target.value })}
						/>
						<input
							type="text"
							className={styles.cardInput}
							placeholder="Address 2"
							value={address2}
							onChange={(e) => this.setState({ address2: e.target.value })}
						/>
						<input
							type="text"
							className={styles.cardInput}
							placeholder="City"
							value={city}
							onChange={(e) => this.setState({ city: e.target.value })}
						/>
						<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
							State:{' '}
							<select
								style={{ marginLeft: 5 }}
								className={styles.cardInput}
								defaultValue={state}
								onChange={(e) => {
									this.setState({ state: e.target.value });
								}}
							>
								{states.map((p) => {
									return (
										<option style={{ background: '#002054' }} value={p.value}>
											{p.label}
										</option>
									);
								})}
							</select>
						</div>
						<input
							type="text"
							className={styles.cardInput}
							placeholder="Zip Code"
							value={zipCode}
							onChange={(e) => this.setState({ zipCode: e.target.value })}
						/>
						<input
							type="number"
							className={styles.cardInput}
							placeholder="Square Feet"
							value={squareFeet}
							onChange={(e) => this.setState({ squareFeet: e.target.value })}
						/>
						<p>
							<b>DETAILS</b>
						</p>
						<select
							style={{ marginLeft: 5 }}
							className={styles.cardInput}
							defaultValue={type}
							placeholder="What type of place is this?"
							onChange={(e) => {
								this.setState({ type: e.target.value });
							}}
						>
							{types.map((type) => {
								return (
									<option style={{ background: '#002054' }} value={type}>
										{type}
									</option>
								);
							})}
						</select>
						{type == 'Other' && (
							<input
								type="text"
								className={styles.cardInput}
								placeholder="What other type of place is this?"
								value={otherType}
								onChange={(e) => this.setState({ otherType: e.target.value })}
							/>
						)}
						<input
							type="number"
							className={styles.cardInput}
							placeholder="# of Rooms Available"
							value={roomsAvailable}
							onChange={(e) => this.setState({ roomsAvailable: e.target.value })}
						/>
						<input
							type="number"
							className={styles.cardInput}
							placeholder="# of Bathrooms Available"
							value={bathroomsAvailable}
							onChange={(e) => this.setState({ bathroomsAvailable: e.target.value })}
						/>

						<textarea
							type="text"
							rows={3}
							style={{ resize: 'none' }}
							className={styles.cardInput}
							placeholder="Describe your place"
							value={description}
							onChange={(e) => this.setState({ description: e.target.value })}
						/>
						<p>
							<b>RENT CYCLE</b>
						</p>
						<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
							<select
								style={{ marginLeft: 5 }}
								className={styles.cardInput}
								defaultValue={rentCycle}
								onChange={(e) => {
									this.setState({ rentCycle: e.target.value });
								}}
							>
								{rentCycles.map((rc) => {
									return (
										<option style={{ background: '#002054' }} value={rc}>
											{rc}
										</option>
									);
								})}
							</select>
						</div>
						<input
							type="text"
							className={styles.cardInput}
							placeholder={`Rent due date${rentCycle == 'bimonthly' ? 's - ex. 1,15' : ''}`}
							value={rentDueDates}
							onChange={(e) => this.setState({ rentDueDates: e.target.value })}
						/>

						<input
							type="number"
							className={styles.cardInput}
							placeholder="Rent amount"
							value={rentAmount}
							onChange={(e) => this.setState({ rentAmount: e.target.value })}
						/>
						<p>
							<b>TENANTS</b>
						</p>
						<div className={styles.tenantList}>
							{tenantKeys.map((tenantKey) => {
								const tenant = tenants[tenantKey];
								return (
									<div className={styles.tenantItem}>
										<div>
											<b>
												{tenant.profile.firstName} {tenant.profile.lastName}
											</b>
											<br />
											{tenant.email}
										</div>
										<div style={{ textAlign: 'right' }}>
											<b>Status</b>
											<br />On Time
										</div>
									</div>
								);
							})}
						</div>
						<br />
						<button disabled={creating} className={styles.primaryButton} onClick={this.createProperty}>
							{!creating ? 'SAVE PROPERTY' : <Loader />}
						</button>
						<button className={styles.secondaryButton} onClick={this.props.close}>
							CLOSE
						</button>
					</div>
				</div>
			</div>
		);
	}
}

export default PropertyDetailsModal;
