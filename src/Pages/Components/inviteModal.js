import React, { Component } from 'react';
import styles from './inviteModal.module.css';
import { user, db } from '../../API/user';
import CryptoJS from 'crypto-js';

function base64url(source) {
	// Encode in classical base64
	let encodedSource = CryptoJS.enc.Base64.stringify(source);

	// Remove padding equal characters
	encodedSource = encodedSource.replace(/=+$/, '');

	// Replace characters according to base64url specifications
	encodedSource = encodedSource.replace(/\+/g, '-');
	encodedSource = encodedSource.replace(/\//g, '_');

	return encodedSource;
}

class InviteModal extends Component {
	constructor(props) {
		super(props);
		const propertyOptions = Object.keys(props.properties).map((p) => {
			const property = props.properties[p];
			return {
				label: `${property.name}`,
				value: p
			};
		});
		const property = propertyOptions[0].value;
		this.state = {
			inviteEmail: '',
			property: property,
			propertyOptions: propertyOptions,
			dueDate: '',
			depositAmount: 0,
			leaseLength: 1,
			message: ''
		};
	}

	/* createPaymentLink = async () => {
		const { inviteEmail, dueDate, leaseLength, depositAmount } = this.state;
		const recipient = new PublicKey(wallet.address);
		const amount = new BigNumber(this.state.amount);
		const reference = new Keypair().publicKey;
		const label = 'R3NT Payment Request';
		const message = 'Pay with R3NT';
		const memo = chosenTenantKey;
		const url = encodeURL({ recipient, amount, reference, label, message, memo });

		const now = new Date().toISOString();
		const transaction = {
			url: url,
			label: label,
			message: message,
			amount: depositAmount,
			status: 'requested',
			type: 'ad-hoc',
			dueDate: dueDate,
			propertyKey: tenant.propertyKey,
			leaseLength: leaseLength,
			dateCreated: now
		};
		console.log(transaction);
		user.get(`transactions-${inviteEmail}`).set(transaction);
	};
 */
	createToken = (data) => {
		var header = {
			alg: 'HS256',
			typ: 'JWT'
		};

		var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
		var encodedHeader = base64url(stringifiedHeader);

		var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
		var encodedData = base64url(stringifiedData);

		var token = encodedHeader + '.' + encodedData;
		return token;
	};

	sendInvitation = () => {
		const { inviteEmail, property, leaseLength, message } = this.state;
		const { profile, email, properties } = this.props;
		const date = new Date().getTime();
		const details = {
			inviteEmail: inviteEmail,
			landLord: { name: `${profile.firstName} ${profile.lastName}`, email: email },
			address: property,
			date: date
		};
		const propertyAddress = `${properties[property].address1} ${properties[property].address2}, ${properties[
			property
		].city} ${properties[property].state} ${properties[property].zipCode}`;
		db.get(`~@${inviteEmail}`).once(async (data, key) => {
			const inviteToken = await db
				.get(`INVITE_${inviteEmail}`)
				.put({
					inviteEmail: inviteEmail,
					landLordPublicKey: this.props.publicKey,
					propertyPublicKey: property,
					date: date,
					propertyName: properties[property].name,
					propertyAddress: propertyAddress,
					propertyType: properties[property].type,
					roomsAvailable: properties[property].roomsAvailable,
					bathroomsAvailable: properties[property].bathroomsAvailable,
					rentDueDates: properties[property].rentDueDates,
					squareFeet: properties[property].squareFeet,
					rentCycle: properties[property].rentCycle,
					rentAmount: properties[property].rentAmount,
					leaseLength: leaseLength,
					message: message,
					accepted: false
				})
				.then();
			details['inviteToken'] = inviteToken;
			if (data) {
				let pubKey = Object.keys(data).find((key) => key != '_');
				if (pubKey && pubKey.startsWith('~')) {
					pubKey = pubKey.substring(1);
					let to = db.user(pubKey);
					user.get('profile').get('firstName').grant(to);
					user.get('profile').get('lastName').grant(to);
					user.get('profile').get('phoneNumber').grant(to);
					user.get('profile').get('rentalProperties').get(property).grant(to);
					user.get(`transactions-${inviteEmail}`).grant(to);
					user.get('rentalProperties').get(property).get('tenants').set({ key: pubKey });
				}
			}
			user.get('tenantInvites').set({ key: inviteToken }).then();
			window.location.href = `mailto:${inviteEmail}?subject=R3NT Tenant Invitation&body=Hi,%0D%0AMy name is ${profile.firstName} ${profile.lastName}.%0D%0AI would like to invite you to be a tenant at%0D%0A%0D%0A${propertyAddress}%0D%0A%0D%0APlease visit the invitation link below to add me as your landlord on R3NT.%0D%0AR3NT is a decentralized progressive web app that will allow us to make property transactions through the solana network.%0D%0A%0D%0ALink%0D%0A${window
				.location.origin}/Invite%3Finvite=${this.createToken(details)}%0D%0A%0D%0AThanks`;
			this.setState({
				sent: true,
				emailLink: `mailto:${inviteEmail}?subject=R3NT Tenant Invitation&body=Hi,%0D%0AMy name is ${profile.firstName} ${profile.lastName}.%0D%0AI would like to invite you to be a tenant at%0D%0A%0D%0A${propertyAddress}%0D%0A%0D%0APlease visit the invitation link below to add me as your landlord on R3NT.%0D%0AR3NT is a decentralized progressive web app that will allow us to make property transactions through the solana network.%0D%0A%0D%0ALink%0D%0A${window
					.location.origin}/Invite%3Finvite=${this.createToken(details)}%0D%0A%0D%0AThanks`,
				inviteLink: `${window.location.origin}/Invite?invite=${this.createToken(details)}`
			});
		});
	};

	render() {
		const {
			inviteEmail,
			property,
			propertyOptions,
			leaseLength,
			message,
			depositAmount,
			sent,
			inviteLink,
			emailLink
		} = this.state;
		const inviteProperty = property && this.props[property];
		return (
			<div className="modalContainer">
				<div className={[ 'modalBox', styles.modalBox ].join(' ')}>
					<div className={styles.modalContent}>
						<h1>Invite Tenant</h1>
						<p>Send an invite email to your tenant to get them set up</p>
						<br />
						<input
							type="email"
							className={styles.cardInput}
							placeholder="Tenant Email"
							value={inviteEmail}
							onChange={(e) => this.setState({ inviteEmail: e.target.value })}
						/>
						<label>Property</label>
						<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
							<select
								style={{ marginLeft: 5, marginBottom: 0 }}
								className={styles.cardInput}
								defaultValue={property}
								onChange={(e) => {
									this.setState({ property: e.target.value });
								}}
							>
								{propertyOptions.map((p) => {
									return (
										<option style={{ background: '#002054' }} value={p.value}>
											{p.label}
										</option>
									);
								})}
							</select>
						</div>
						<p>
							<strong style={{ color: '#002054', textShadow: '0 0 3px #00ffff' }}>
								RENTAL AGREEMENT
							</strong>
						</p>
						<p>Rent Cycle: {inviteProperty ? inviteProperty.rentCycle : ''}</p>
						<input
							type="number"
							className={styles.cardInput}
							placeholder="Lease length - how many sycles will this be available for"
							value={leaseLength}
							onChange={(e) => this.setState({ leaseLength: e.target.value })}
						/>

						<input
							type="text"
							className={styles.cardInput}
							placeholder="Invite message - welcome tenant and set rules"
							value={message}
							onChange={(e) => this.setState({ message: e.target.value })}
						/>
						<br />
						{sent && (
							<a className={styles.link} href={inviteLink}>
								COPY INVITE LINK
							</a>
						)}
						{!sent ? (
							<button className={styles.primaryButton} onClick={this.sendInvitation}>
								SEND INVITE
							</button>
						) : (
							<button onClick={() => window.open(emailLink, '_blank')} className={styles.primaryButton}>
								OPEN MAIL
							</button>
						)}
						<button className={styles.secondaryButton} onClick={this.props.close}>
							CLOSE
						</button>
					</div>
				</div>
			</div>
		);
	}
}

export default InviteModal;
