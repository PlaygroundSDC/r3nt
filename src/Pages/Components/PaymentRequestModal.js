import React, { Component } from 'react';
import { Keypair, Cluster, clusterApiUrl, Connection, PublicKey } from '@solana/web3.js';
import { encodeURL, createQR, createTransaction } from '@solana/pay';
import BigNumber from 'bignumber.js';
import { user, db } from '../../API/user';
import styles from './paymentRequestModal.module.css';

const roleOptions = [ { label: 'Landlord', value: 'landlord' }, { label: 'Tenant', value: 'tenant' } ];

const walletOptions = [
	{ label: 'Phantom', value: 'phantom' },
	{ label: 'Crypto Please', value: 'cryptoplease' },
	{ label: 'FTX', value: 'ftx' }
];

class PaymentRequestModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			paymentLink: '',
			amount: 0,
			wallet: props.wallet,
			chosenTenantKey: ''
		};
	}

	createPaymentLink = async () => {
		const { wallet, chosenTenantKey, dueDate, leaseLength } = this.state;
		if (chosenTenantKey != '') {
			const tenant = this.props.tenants[chosenTenantKey];
			const recipient = new PublicKey(wallet.address);
			const amount = new BigNumber(this.state.amount);
			const reference = new Keypair().publicKey;
			const label = 'R3NT Payment Request';
			const message = 'Pay with R3NT';
			const memo = chosenTenantKey;
			const url = encodeURL({ recipient, amount, reference, label, message, memo });

			const now = new Date().toISOString();
			const transaction = {
				url: url,
				label: label,
				message: message,
				amount: amount,
				status: 'requested',
				type: 'ad-hoc',
				dueDate: dueDate,
				propertyKey: tenant.propertyKey,
				leaseLength: leaseLength,
				dateCreated: now,
				payerKey: chosenTenantKey
			};
			console.log(transaction);
			/* let to = db.user(chosenTenantKey);
			user.get(`transactions-${chosenTenantKey}`).set(transaction);
			user.get(`transactions-${chosenTenantKey}`).grant(to);
			this.setState({ paymentLink: url }); */
		}
	};

	render() {
		const { wallet, chosenTenantKey, paymentLink, amount } = this.state;
		const { tenants } = this.props;
		const tenantKeys = Object.keys(tenants);
		return (
			<div className="modalContainer">
				<div className={[ 'modalBox', styles.modalBox ].join(' ')}>
					{paymentLink == '' ? (
						<div className={styles.modalContent}>
							<h1>PAYMENT REQUEST</h1>
							<p>Choose a tenant and amount you wish to get paid</p>
							<label>Tenant</label>
							<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
								<select
									style={{ marginLeft: 5 }}
									className={[ styles.cardInput, styles.cardSelect ].join(' ')}
									defaultValue={chosenTenantKey}
									onChange={(e) => {
										this.setState({
											chosenTenantKey: e.target.value
										});
									}}
								>
									<option style={{ background: '#002054' }} value={''} />
									{tenantKeys.map((tenantKey) => {
										const tenant = tenants[tenantKey];
										return (
											<option style={{ background: '#002054' }} value={tenantKey}>
												{tenant.profile.firstName} {tenant.profile.lastName}
											</option>
										);
									})}
								</select>
							</div>
							<label>Amount</label>
							<input
								type="number"
								className={styles.cardInput}
								placeholder="Square Feet"
								value={amount}
								onChange={(e) => this.setState({ amount: e.target.value })}
							/>

							<label>Due Date</label>
							<input
								type="number"
								className={styles.cardInput}
								placeholder="Square Feet"
								value={amount}
								onChange={(e) => this.setState({ amount: e.target.value })}
							/>

							<label>Lease Length</label>
							<input
								type="number"
								className={styles.cardInput}
								placeholder="Number of cycles being rented. ex 3 biweekly or 2 monthly"
								value={amount}
								onChange={(e) => this.setState({ amount: e.target.value })}
							/>
							{wallet &&
							wallet.type != '' && (
								<div style={{ marginBottom: 20 }}>
									<label>Wallet</label>
									<div>{wallet.type}</div>
									<div>{wallet.address}</div>
								</div>
							)}
							<button className={styles.primaryButton} onClick={this.createPaymentLink}>
								SEND
							</button>
							<button className={styles.secondaryButton} onClick={this.props.close}>
								CLOSE
							</button>
						</div>
					) : (
						<div>
							<h1>PAYMENT LINK CREATED</h1>
							<p>Your payment link is ready, send it to your tenant to request payment</p>
							<a href={paymentLink}>{paymentLink}</a>
							<br />
							<button className={styles.primaryButton} onClick={this.createTransaction}>
								SEND REQUEST
							</button>
							<button className={styles.secondaryButton} onClick={this.props.close}>
								CANCEL
							</button>
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default PaymentRequestModal;
