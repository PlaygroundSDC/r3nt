import React, { Component } from 'react';
import { user, db } from '../../API/user';
import styles from './profileModal.module.css';

import ftx_icon from '../../assets/images/ftx-icon.png';
import cryptoPlease_icon from '../../assets/images/cryptoplease_icon.jpeg';
import phantom_icon from '../../assets/images/phantom_icon.png';

const roleOptions = [ { label: 'Landlord', value: 'landlord' }, { label: 'Tenant', value: 'tenant' } ];

const walletOptions = [
	{ label: 'Phantom', value: 'phantom' },
	{ label: 'Crypto Please', value: 'cryptoplease' },
	{ label: 'FTX', value: 'ftx' }
];

class ProfileModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstName: props.profile.firstName,
			lastName: props.profile.lastName,
			role: props.profile.role,
			wallet: props.wallet
		};
	}

	logout = async () => {
		await user.leave();
		window.location.reload();
	};

	updateUser = async () => {
		const { firstName, lastName, role, wallet } = this.state;
		if (role != this.props.profile.role) {
			await user.get('profile').put({ firstName: firstName, lastName: lastName, role: role }).then(() => {
				window.location.reload(false);
			});
		} else {
			await user.get('profile').put({ firstName: firstName, lastName: lastName }).then();
		}
		if (wallet != this.props.wallet) {
			await user.get('wallet').put({ type: wallet.type, address: wallet.address }).then();
		}
		//await user.get('wallet').then()
		this.props.close();
	};

	render() {
		const { firstName, lastName, role, wallet } = this.state;

		const initials = `${firstName && firstName != '' ? firstName.toUpperCase().charAt(0) : ''}${lastName &&
		lastName != ''
			? lastName.toUpperCase().charAt(0)
			: ''}`;
		return (
			<div className="modalContainer">
				<div className={[ 'modalBox', styles.modalBox ].join(' ')}>
					<div className={[ 'modalContent', styles.modalContent ].join(' ')}>
						<h1>Profile</h1>
						<div
							className={styles.profileImage}
							onClick={() => {
								this.setState({ profileModalOpen: true });
							}}
						>
							<div className={styles.initials}>{initials}</div>
						</div>
						<div className={styles.cardInputLabel}>{this.props.email}</div>
						<input
							className={styles.cardInput}
							placeholder="First Name"
							value={firstName}
							onChange={(e) => this.setState({ firstName: e.target.value })}
						/>
						<input
							className={styles.cardInput}
							placeholder="Last Name"
							value={lastName}
							onChange={(e) => this.setState({ lastName: e.target.value })}
						/>
						<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
							Role:
							<select
								style={{ marginLeft: 5 }}
								className={[ styles.cardInput, styles.cardSelect ].join(' ')}
								defaultValue={role}
								onChange={(e) => {
									this.setState({ role: e.target.value });
								}}
							>
								{roleOptions.map((role) => {
									return (
										<option style={{ background: '#002054' }} value={role.value}>
											{role.label}
										</option>
									);
								})}
							</select>
						</div>

						<div className={styles.cardInputLabel} style={{ display: 'flex' }}>
							{/*  <select style={{marginLeft:5}} className={[styles.cardInput, styles.cardSelect].join(' ')} defaultValue={wallet ? wallet.type : ''} onChange={(e)=> {this.setState({wallet: {...this.state.wallet,type:e.target.value}})}}> 
                        <option style={{background:"#002054"}} value={""}></option>
                        {walletOptions.map(walletType => {    
                            return <option style={{background:"#002054"}} value={walletType.value}>{walletType.label}</option>
                        })}
                        </select> */}
							<div>
								<div className={styles.walletName}>Wallet</div>
								<div className={styles.logoContainer}>
									<div className={styles.walletContainer}>
										<img
											src={phantom_icon}
											onClick={() => {
												this.setState({ wallet: { ...this.state.wallet, type: 'phantom' } });
											}}
											style={{ margin: 0 }}
											className={wallet.type == 'phantom' && styles.walletSelected}
										/>
									</div>

									<div>
										<img
											src={cryptoPlease_icon}
											className={wallet.type == 'cryptoplease' && styles.walletSelected}
											onClick={() => {
												this.setState({
													wallet: { ...this.state.wallet, type: 'cryptoplease' }
												});
											}}
										/>
									</div>

									<div>
										<img
											src={ftx_icon}
											className={wallet.type == 'ftx' && styles.walletSelected}
											onClick={() => {
												this.setState({ wallet: { ...this.state.wallet, type: 'ftx' } });
											}}
										/>
									</div>
								</div>
								<div className={styles.walletName}>{wallet.type}</div>
							</div>
						</div>
						{wallet &&
						wallet.type != '' && (
							<div>
								<input
									className={styles.cardInput}
									placeholder="Wallet Address"
									value={wallet.address}
									onChange={(e) =>
										this.setState({ wallet: { ...this.state.wallet, address: e.target.value } })}
								/>
							</div>
						)}
						<button className={styles.primaryButton} onClick={this.updateUser}>
							SAVE
						</button>
						<button className={styles.secondaryButton} onClick={this.props.close}>
							CLOSE
						</button>

						<button style={{ color: '#9b0000' }} className={styles.secondaryButton} onClick={this.logout}>
							LOG OUT
						</button>
					</div>
				</div>
			</div>
		);
	}
}

export default ProfileModal;
