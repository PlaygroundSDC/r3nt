import React, { Component } from 'react';
import styles from './tenant.module.css';
import { user, db } from '../../API/user';
import ProfileModal from '../Components/ProfileModal';
import moment from 'moment';
import { Keypair, Cluster, clusterApiUrl, Connection, PublicKey } from '@solana/web3.js';
import { encodeURL, createQR, createTransaction, parseURL } from '@solana/pay';
import BigNumber from 'bignumber.js';

const roleOptions = [ { label: 'Landlord', value: 'landlord' }, { label: 'Tenant', value: 'tenant' } ];
class Tenant extends Component {
	constructor(props) {
		super(props);
		console.log(props.wallet);
		this.state = {
			profileModalOpen: false,
			property: {},
			landlordProfile: {},
			payments: {},
			landlordEmail: '',
			loading: true
		};
	}

	createTransaction = async (url) => {
		const { wallet } = this.props;
		const { recipient, amount, splToken, reference, label, message, memo } = parseURL(url);

		const connection = new Connection(clusterApiUrl('devnet'), 'confirmed');
		const payer = new PublicKey(wallet.address);

		const tx = await createTransaction(connection, payer, recipient, amount, {
			reference,
			memo
		});

		console.log(tx);
		//sendAndConfirmTransaction(connection, tx, [CUSTOMER_WALLET]);
	};

	async componentDidMount() {
		var landlordKey = await user.get('landlord').get('key').then();
		var landlordprofile = await db.user(landlordKey).get('profile').then();
		var landlordEmail = await db.user(landlordKey).get('alias').then();
		if (landlordprofile) {
			this.setState({ landlordProfile: landlordprofile, landlordEmail: landlordEmail });
			var propertyKey = await user.get('property').get('key').then();
			console.log(propertyKey);
			var property = await db.user(landlordKey).get('rentalProperties').get(propertyKey).then();
			console.log(property);
			this.setState({ property: property, landlordProfile: landlordprofile, loading: false });
			await db.user(landlordKey).get(`transactions-${this.props.publicKey}`).map().on((transaction, id) => {
				var payments = this.state.payments;
				payments[id] = transaction;
				this.setState({ payments: payments });
			});
		} else {
			this.setState({ property: null, landlordProfile: null, loading: false });
		}
	}

	updateRole = async () => {
		const { profile } = this.props;
		//console.log(this.state)

		await user
			.get('profile')
			.put({ firstName: profile.firstName, lastName: profile.lastName, role: 'landlord' })
			.then();

		//this.props.close();
	};

	render() {
		const { profile, wallet, publicKey } = this.props;
		const { profileModalOpen, landlordProfile, property, payments, loading, landlordEmail } = this.state;
		const initials = `${profile.firstName && profile.firstName != ''
			? profile.firstName.toUpperCase().charAt(0)
			: ''}${profile.lastName && profile.lastName != '' ? profile.lastName.toUpperCase().charAt(0) : ''}`;
		const paymentKeys = Object.keys(payments);
		return (
			<div className="page">
				<div className={styles.masterContainer}>
					<div className={styles.appContainer}>
						<div className={styles.dashboardContainer}>
							<div className={styles.topWidget}>
								<div className={styles.profileBox}>
									<div className={styles.pageTitle}>R3NT</div>
									<div
										className={styles.profileImage}
										onClick={() => {
											console.log('push');
											this.setState({ profileModalOpen: true });
										}}
									>
										<div className={styles.initials}>{initials}</div>
									</div>
								</div>
								<div className={styles.profileBox2}>
									<div className={styles.profileName}>
										Hello, {profile.firstName} {profile.lastName}
									</div>
								</div>
							</div>
						</div>

						{!loading && property ? (
							<div className={styles.landlordInformationContainer}>
								<div className={styles.middleWidget}>
									<div className={styles.widgetTitle}>Property Information</div>
									<div className={styles.profileBox3}>
										<div className={styles.landLordInformation}>
											<div className={styles.landlordName}>{property.name}</div>
											<div className={styles.profileSubtitle}>
												{/* 3178 camden place, beaufort, South Carolina SC 29902 */}
												{property.address1}, {property.city}, {property.state},{' '}
												{property.zipCode}
											</div>
											<div className={styles.profileSubtitle}>{property.description}</div>
										</div>
									</div>

									<div className={styles.widgetTitle}>Landlord</div>
									<div className={styles.profileBox3}>
										<div className={styles.landLordInformation}>
											<div className={styles.landlordName}>
												{landlordProfile.firstName} {landlordProfile.lastName}
											</div>
											<div className={styles.profileSubtitle}>
												{/* 3178 camden place, beaufort, South Carolina SC 29902 */}
												{landlordEmail}
											</div>
										</div>
									</div>
									<div className={styles.landlordButtonContainer}>
										{landlordProfile.phoneNumber && (
											<a
												className={styles.secondaryButton}
												style={{ marginRight: 5 }}
												href="tel:+900300400"
											>
												{landlordProfile.phoneNumber}
											</a>
										)}
										<a
											className={styles.primaryButton}
											style={{ marginLeft: 5 }}
											href={`mailto:${landlordEmail}`}
										>
											SEND EMAIL
										</a>
									</div>
								</div>
							</div>
						) : (
							<div className={styles.landlordInformationContainerNoLandlord}>
								<div className={styles.middleWidgetNoLandlord}>
									<div className={styles.widgetTitle}>
										You dont have a landlord. Please wait it until you recieve an invite
									</div>
								</div>
							</div>
						)}

						<div className={styles.paymentOptionsContainer}>
							<div className={styles.paymentWidget}>
								<div className={styles.paymentHeaderOptions}>
									<div className={styles.payment}>Payments</div>

									<button
										className={styles.primaryButton}
										style={{ margin: 0, width: 100 }}
										onClick={() => this.setState({ transactionHistoryModalOpen: true })}
									>
										HISTORY
									</button>
								</div>
								<div className={styles.listContainer}>
									{paymentKeys.length == 0 && (
										<div className={styles.listItemContainer}>
											<div className={styles.tenantBox}>
												<div className={styles.landLordInformation}>
													<div
														style={{ fontSize: 30, color: '#002054' }}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														}}
													>
														MAKE PAYMENTS
													</div>
													<div className={styles.profileSubtitle}>
														Incoming payment requests will be listed here as they become
														due.
													</div>
												</div>
											</div>
										</div>
									)}
									{paymentKeys.map((paymentKey) => {
										const payment = payments[paymentKey];
										const paymentDate = moment(payment.date);

										return (
											<div className={styles.listItemContainer}>
												<h3>{paymentDate.format('MMMM')}</h3>
												<div className={styles.rentData}>
													<div className={styles.rentLabels}>
														<p>Amount</p>
														<p>Date due</p>
														<p>Requestor</p>
													</div>
													<div className={styles.rentValues}>
														<p>${payment.amount}</p>
														<p>{paymentDate.format('MM/DD/YYYY')}</p>
														<p />
													</div>
												</div>
												<div className={styles.listItemButtonContainer}>
													<button
														className={styles.primaryButton}
														onClick={() => this.createTransaction(payment.url)}
													>
														PAY
													</button>
												</div>
											</div>
										);
									})}
								</div>
							</div>
						</div>

						{/* <div className={styles.securityDepositContainer}>
							<div className={styles.securityDepositHeader}>Security Deposit</div>
							<div className={styles.securityDepositWidget}>
								<div className={styles.dollarAmount}>$10,000</div>
								<div className={styles.text}>5 months Deposit</div>
							</div>
						</div> */}
						{profileModalOpen && (
							<ProfileModal
								profile={profile}
								wallet={wallet}
								publicKey={publicKey}
								email={this.props.email}
								close={() => this.setState({ profileModalOpen: false })}
							/>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default Tenant;
