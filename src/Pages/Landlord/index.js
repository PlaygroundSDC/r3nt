import React, { Component } from 'react';
import { user, db } from '../../API/user';
import AddPropertyModal from '../Components/addPropertyModal';
import InviteModal from '../Components/inviteModal';
import PaymentRequestModal from '../Components/PaymentRequestModal';
import ProfileModal from '../Components/ProfileModal';
import PropertyDetailsModal from '../Components/propertyDetailsModal';
import styles from './landlord.module.css';
import moment from 'moment';

const iconMap = {
	Room: <i className="las la-door-closed" />,
	House: <i className="las la-home" />,
	Apartment: <i className="las la-building" />,
	Condo: <i className="las la-city" />,
	Boat: <i className="las la-anchor" />,
	Shed: <i className="las la-warehouse" />,
	Other: <i className="las la-igloo" />
};

class Landlord extends Component {
	constructor(props) {
		super(props);
		this.state = {
			invites: {},
			tenants: {},
			tenantDetailsOpen: false,
			focusedTenant: null,
			properties: {},
			propertyDetailsOpen: false,
			focusedProperty: null,
			payments: {},
			paymentDetailsOpen: false,
			focusedPayment: null,
			complaints: {},
			complaintDetailsOpen: false,
			focusedComplaint: null,
			documents: {},
			documentDetailsOpen: false,
			focusedDocument: null,
			profileModalOpen: false,
			paymentRequestModalOpen: false,
			inviteModalOpen: false,
			addPropertyModalOpen: false,
			paymentsModalOpen: false,
			complaintsModalOpen: false,
			documentsModalOpen: false
		};
	}

	/* getTenants = async () => {
		var pubs = (await user.get('tenants').then()) || [];
		const tenants = [];
		for (let p = 0; p < pubs.length; p++) {
			var to = db.user(pubs[p]);
			var who = await to.get('alias').then();
			var profile = await to.get('profile').then();
			tenants.push({ email: who, profile: profile });
		}
		this.setState({ tenants: tenants });
	}; */

	componentDidMount() {
		this.setState({ loadingProperties: true });
		user.get('tenantInvites').map().on((invite) => {
			db.get(invite.key).then((invitation) => {
				//console.log('INVITATION', invitation);
				const tenantInviteEmail = invitation.inviteEmail;
				const invites = this.state.invites;
				invites[invite.key] = invitation;
				this.setState({ invites: invites });
				const tenants = this.state.tenants;

				db.get(`~@${tenantInviteEmail}`).once(async (data) => {
					if (data) {
						let pubKey = Object.keys(data).find((key) => key != '_');
						if (pubKey && pubKey.startsWith('~')) {
							pubKey = pubKey.substring(1);
							let to = await db.user(pubKey).get('profile').then();
							if (to) {
								const tenants = this.state.tenants;
								if (tenants[pubKey]) {
									tenants[pubKey] = {
										...tenants[pubKey],
										accepted: true,
										invite: invite.key
									};
								} else {
									tenants[pubKey] = {
										email: tenantInviteEmail,
										profile: to,
										accepted: invite.accepted,
										inactive: false,
										invite: invite.key
									};
								}
								this.setState({ tenants: tenants });
							}
						}
					} else if (tenantInviteEmail && !tenants[tenantInviteEmail]) {
						tenants[tenantInviteEmail] = {
							inactive: true,
							email: tenantInviteEmail,
							date: invite.date,
							propertyName: invite.propertyName
						};
						this.setState({ tenants: tenants });
					}
				});
			});
			/*  */
		});
		user.get('rentalProperties').map().once((property, id) => {
			if (property) {
				const properties = this.state.properties;
				properties[id] = property;
				this.setState({ properties: properties, loadingProperties: false });
				user.get('rentalProperties').get(id).get('tenants').map().on(async (tenant) => {
					const tenantProfile = await db.user(tenant.key).get('profile').then();
					const tenantEmail = await db.user(tenant.key).get('alias').then();
					const tenantPropertyKey = await db.user(tenant.key).get('property').get('key').then();
					const tenantProperty = await user.get('rentalProperties').get(tenantPropertyKey).then();
					user.get(`transactions-${tenant.key}`).map().once((transaction, tid) => {
						const payments = this.state.payments;
						payments[tid] = transaction;
						this.setState({ payments: payments });
					});
					if (tenantProfile) {
						const tenants = this.state.tenants;
						if (tenants[tenant.key]) {
							tenants[tenant.key] = {
								...tenants[tenant.key],
								email: tenantEmail,
								profile: tenantProfile,
								property: tenantProperty,
								propertyKey: tenantPropertyKey
							};
						} else {
							tenants[tenant.key] = {
								accepted: true,
								email: tenantEmail,
								profile: tenantProfile,
								property: tenantProperty,
								propertyKey: tenantPropertyKey
							};
						}
						this.setState({ tenants: tenants });
					}
				});
			}
		});
	}

	refreshProperties = () => {
		user.get('rentalProperties').map().once((property, id) => {
			if (property) {
				const properties = this.state.properties;
				properties[id] = property;
				this.setState({ properties: properties, loadingProperties: false });
				user.get('rentalProperties').get(id).get('tenants').map().on((tenant, id) => {
					const tenants = this.state.tenants;
					tenants[id] = tenant;
					this.setState({ tenants: tenants, loadingTenants: false });
				});
			}
		});
	};

	removeProperty = async (id) => {
		await user.get('rentalProperties').get(id).put(null).then();
		const properties = this.state.properties;
		delete properties[id];
		this.setState({ properties: properties });
	};

	openPropertyDetails = (key) => {
		this.setState({ propertyDetailsOpen: true, focusedProperty: { id: key, ...this.state.properties[key] } });
	};

	authorizeTenant = (tenantKey, invite) => {
		let to = db.user(tenantKey);
		console.log(tenantKey, invite);

		/* user.get('profile').get('firstName').grant(to);
		user.get('profile').get('lastName').grant(to);
		user.get('profile').get('phoneNumber').grant(to);
		user.get('profile').get('rentalProperties').get(property).grant(to);
		user.get(`transactions-${inviteEmail}`).grant(to);
		user.get('rentalProperties').get(property).get('tenants').set({ key: pubKey }); */
	};

	render() {
		const { firstName, lastName } = this.props.profile;
		const {
			tenants,
			properties,
			payments,
			complaints,
			documents,
			profileModalOpen,
			inviteModalOpen,
			paymentRequestModalOpen,
			addPropertyModalOpen,
			paymentsModalOpen,
			complaintsModalOpen,
			documentsModalOpen,
			tenantDetailsOpen,
			focusedTenant,
			propertyDetailsOpen,
			focusedProperty,
			complaintDetailsOpen,
			focusedComplaint,
			paymentDetailsOpen,
			focusedPayment,
			documentDetailsOpen,
			focusedDocument
		} = this.state;
		const initials = `${firstName && firstName != '' ? firstName.toUpperCase().charAt(0) : ''}${lastName &&
		lastName != ''
			? lastName.toUpperCase().charAt(0)
			: ''}`;
		const tenantKeys = Object.keys(tenants);
		const propertyKeys = Object.keys(properties);
		const paymentKeys = Object.keys(payments);
		const complaintKeys = Object.keys(complaints);
		const documentKeys = Object.keys(documents);
		return (
			<div className="page">
				<div className={styles.dashboardContainer}>
					<div className={styles.appContainer}>
						<div className={styles.topWidget}>
							<div className={styles.profileBox}>
								<div className={styles.pageTitle}>R3NT</div>
								<div
									className={styles.profileImage}
									onClick={() => {
										this.setState({ profileModalOpen: true });
									}}
								>
									<div className={styles.initials}>{initials}</div>
								</div>
							</div>
							<div className={styles.dashboardHeader}>
								<div className={styles.profileName}>Welcome, {firstName}</div>
								<div>
									<button
										className={styles.primaryButton}
										onClick={() => this.setState({ paymentRequestModalOpen: true })}
									>
										SEND PAYMENT REQUEST
									</button>
								</div>
							</div>
						</div>
						<div className={styles.landlordInformationContainer}>
							<div className={styles.middleWidget}>
								<div className={styles.landlordButtonContainer} style={{ padding: 20 }}>
									<div style={{ padding: 0 }} className={styles.widgetTitle}>
										TENANTS
									</div>
									{tenantKeys.length > 0 && (
										<button
											className={styles.primaryButton}
											style={{ margin: 0, width: 100 }}
											onClick={() => {
												this.setState({ inviteModalOpen: true });
											}}
										>
											INVITE
										</button>
									)}
								</div>
								<div className={styles.tenantsContainer}>
									{tenantKeys.length == 0 && (
										<div className={styles.tenantContainer}>
											<div className={styles.tenantBox}>
												<div className={styles.landLordInformation}>
													<div
														className={styles.landlordName}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														}}
													>
														ADD TENANTS
													</div>
													<div className={styles.profileSubtitle}>
														Send an invite email to your tenant to get them set up
													</div>
												</div>
											</div>
											{propertyKeys.length > 0 && (
												<div className={styles.landlordButtonContainer}>
													<button
														className={styles.primaryButton}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														} /* window.open(`mailto:${tenant.email}`) */}
													>
														INVITE
													</button>
												</div>
											)}
										</div>
									)}
									{tenantKeys.map((tenantKey) => {
										const tenant = tenants[tenantKey];
										tenant.invite && console.log(this.state.invites[tenant.invite]);
										const tenantInitials =
											tenant.accepted && tenant.profile
												? `${tenant.profile.firstName && tenant.profile.firstName != ''
														? tenant.profile.firstName.toUpperCase().charAt(0)
														: ''}${tenant.profile.lastName && tenant.profile.lastName != ''
														? tenant.profile.lastName.toUpperCase().charAt(0)
														: ''}`
												: tenant.email ? tenant.email.charAt(0) : '';
										return (
											<div className={styles.tenantContainer}>
												<div>
													<div className={styles.tenantBox}>
														<div className={styles.profileImage}>
															<div className={styles.initials}>{tenantInitials}</div>
														</div>
														{!tenant.inactive && tenant.profile ? (
															<div className={styles.landLordInformation}>
																<div className={styles.landlordName}>
																	{tenant.profile.firstName} {tenant.profile.lastName}
																</div>
															</div>
														) : (
															<div className={styles.landLordInformation}>
																<div className={styles.landlordName}>
																	{tenant.email}
																</div>
															</div>
														)}
													</div>
													{tenant.property ? (
														<div className={styles.profileSubtitle}>
															Living in {tenant.property.name}
															<br />
															{/* <br />
															<div
																className={[
																	styles.secondaryColor,
																	styles.flexSB
																].join(' ')}
															>
																<strong>Due: </strong>
																{tenant.property.due} <strong>By: </strong>
																{tenant.property.dueBy}
															</div> */}
															<br />
														</div>
													) : tenant.profile ? (
														<div className={styles.profileSubtitle}>
															Tenant created account and accepted agreement.
															<br />Finish the process by giving access and setting up
															first payment
															<br />
															<br />
														</div>
													) : (
														!tenant.accepted && (
															<div className={styles.profileSubtitle}>
																{!tenant.accepted && <div>Invitation pending</div>}
																<br />Tenant has not accepted invitation agreement yet.
																<br />
																<br />
															</div>
														)
													)}
												</div>
												<div className={styles.landlordButtonContainer}>
													{!tenant.property &&
													tenant.profile && (
														<button
															className={styles.secondaryButton}
															style={{ color: 'white' }}
															onClick={() =>
																this.authorizeTenant(
																	tenantKey,
																	this.state.invites[tenant.invite]
																)}
														>
															R3NT
														</button>
													)}
													{tenant.profile &&
													tenant.profile.phoneNumber && (
														<span>
															<button
																className={styles.secondaryButton}
																onClick={() =>
																	window.open(`phone:${tenant.profile.phoneNumber}`)}
															>
																CALL
															</button>
														</span>
													)}
													{(tenant.profile && tenant.profile.phoneNumber) ||
														(!tenant.property &&
														tenant.profile && <div style={{ minWidth: 10 }} />)}
													<button
														className={styles.primaryButton}
														onClick={() => window.open(`mailto:${tenant.email}`, '_blank')}
													>
														SEND EMAIL
													</button>
												</div>
											</div>
										);
									})}
								</div>
								<div className={styles.landlordButtonContainer} style={{ padding: 20, paddingTop: 0 }}>
									<div style={{ padding: 0 }} className={styles.widgetTitle}>
										PROPERTIES
									</div>
									{propertyKeys.length > 0 && (
										<button
											className={styles.primaryButton}
											style={{ margin: 0, width: 100 }}
											onClick={() => {
												this.setState({ addPropertyModalOpen: true });
											}}
										>
											ADD
										</button>
									)}
								</div>

								<div className={styles.tenantsContainer}>
									{propertyKeys.length == 0 && (
										<div className={styles.tenantContainer}>
											<div className={styles.tenantBox}>
												<div className={styles.landLordInformation}>
													<div
														className={styles.landlordName}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														}}
													>
														MANAGE PROPERTIES
													</div>
													<div className={styles.profileSubtitle}>
														Add address, tenant capacity, and rental agreements to set up
													</div>
												</div>
											</div>
											<div className={styles.landlordButtonContainer}>
												<button
													className={styles.primaryButton}
													onClick={() => {
														this.setState({ addPropertyModalOpen: true });
													}}
												>
													ADD
												</button>
											</div>
										</div>
									)}
									{propertyKeys.map((propertyKey) => {
										const property = properties[propertyKey];
										return (
											<div className={styles.tenantContainer}>
												<div>
													<div className={styles.tenantBox}>
														<div className={styles.profileImage}>
															{/* <div className={styles.initials}>{tenantInitials}</div> */}
															{iconMap[property.type] ? (
																iconMap[property.type]
															) : (
																<i className="las la-igloo" />
															)}
														</div>
														<div className={styles.landLordInformation}>
															<div className={styles.landlordName}>{property.name}</div>
														</div>
													</div>
													<div className={styles.profileSubtitle}>
														{property.address1}, {property.address2}, {property.city}{' '}
														{property.state} {property.postalCode}
														<br />
														<br />
														<div
															className={[ styles.secondaryColor, styles.flexSB ].join(
																' '
															)}
														>
															<strong>Status: </strong>
															{property.tenants ? 'Rented' : 'Available'}
														</div>
														<br />
													</div>
												</div>
												<div className={styles.landlordButtonContainer}>
													{/* <button
														className={styles.secondaryButton}
														onClick={() => this.removeProperty(propertyKey)}
													>
														DELETE
													</button> */}
													<button
														className={styles.primaryButton}
														onClick={() => this.openPropertyDetails(propertyKey)}
													>
														VIEW
													</button>
													{/* <div style={{ minWidth: 10 }} />
													<button
														className={styles.primaryButton}
														onClick={() => window.open(`mailto:${tenant.email}`, '_blank')}
													>
														Send Email
													</button> */}
												</div>
											</div>
										);
									})}
								</div>
								<div className={styles.landlordButtonContainer} style={{ padding: 20, paddingTop: 0 }}>
									<div style={{ padding: 0 }} className={styles.widgetTitle}>
										PAYMENTS
									</div>
									<button
										className={styles.primaryButton}
										style={{ margin: 0, width: 100 }}
										onClick={() => {}}
									>
										HISTORY
									</button>
								</div>

								<div className={styles.tenantsContainer}>
									{paymentKeys.length == 0 && (
										<div className={styles.tenantContainer}>
											<div className={styles.tenantBox}>
												<div className={styles.landLordInformation}>
													<div
														className={styles.landlordName}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														}}
													>
														MANAGE PAYMENTS
													</div>
													<div className={styles.profileSubtitle}>
														View payments recieved from tenants as well as requests made for
														payment
													</div>
												</div>
											</div>
											<div className={styles.landlordButtonContainer}>
												<button
													className={styles.primaryButton}
													onClick={() => {
														this.setState({ inviteModalOpen: true });
													} /* window.open(`mailto:${tenant.email}`) */}
												>
													REQUEST
												</button>
											</div>
										</div>
									)}
									{paymentKeys.map((paymentKey) => {
										const payment = payments[paymentKey];
										const payer = tenants[payment.payerKey];
										return (
											<div className={styles.tenantContainer}>
												<div className={styles.tenantBox} style={{ justifyContent: 'center' }}>
													<div className={styles.landLordInformation} style={{ margin: 0 }}>
														<div className={styles.landlordName}>
															{payer.profile.firstName} {payer.profile.lastName}
														</div>
														{payment.status}
														<span style={{ fontSize: 14 }}>
															{moment(payment.date).format('MM/DD/YYYY h:m:s')}
														</span>
													</div>
												</div>
												{/* <div>
													
													<div className={styles.profileSubtitle}>
														{tenant.address.first}, {tenant.address.second},{' '}
														{tenant.address.city} {tenant.address.state}{' '}
														{tenant.address.postalCode}
														<br />
														<br />
														<div
															className={[ styles.secondaryColor, styles.flexSB ].join(
																' '
															)}
														>
															<strong>Due: </strong>
															{tenant.address.due} <strong>By: </strong>
															{tenant.address.dueBy}
														</div>
														<br />
													</div>
												</div>
												<div className={styles.landlordButtonContainer}>
													<button
														className={styles.secondaryButton}
														onClick={() =>
															window.open(`phone:${tenant.profile.phoneNumber}`)}
													>
														Call
													</button>
													<div style={{ minWidth: 10 }} />
													<button
														className={styles.primaryButton}
														onClick={() => window.open(`mailto:${tenant.email}`, '_blank')}
													>
														Send Email
													</button>
												</div> */}
											</div>
										);
									})}
								</div>
								{/* <div className={styles.landlordButtonContainer} style={{ padding: 20, paddingTop: 0 }}>
									<div style={{ padding: 0 }} className={styles.widgetTitle}>
										COMPLAINTS
									</div>
									<button
										className={styles.primaryButton}
										style={{ margin: 0, width: 100 }}
										onClick={() => {}}
									>
										FILE NEW
									</button>
								</div> */}

								{/* <div className={styles.tenantsContainer}>
									{complaintKeys.length == 0 && (
										<div className={styles.tenantContainer}>
											<div className={styles.tenantBox}>
												<div className={styles.landLordInformation}>
													<div
														className={styles.landlordName}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														}}
													>
														MANAGE COMPLAINTS
													</div>
													<div className={styles.profileSubtitle}>
														Tenants will send these to formally complain, you can also add
														some yourself for accountability
													</div>
												</div>
											</div>
											<div className={styles.landlordButtonContainer}>
												<button
													className={styles.primaryButton}
													onClick={() => {
														this.setState({ inviteModalOpen: true });
													}}
												>
													FILE NEW
												</button>
											</div>
										</div>
									)}
									{complaintKeys.map((complaintKey) => {
										const tenant = complaints[complaintKey];
										const tenantInitials = `${tenant.profile.firstName &&
										tenant.profile.firstName != ''
											? tenant.profile.firstName.toUpperCase().charAt(0)
											: ''}${tenant.profile.lastName && tenant.profile.lastName != ''
											? tenant.profile.lastName.toUpperCase().charAt(0)
											: ''}`;
										return (
											<div className={styles.tenantContainer}>
												<div>
													<div className={styles.tenantBox}>
														<div className={styles.profileImage}>
															<div className={styles.initials}>{tenantInitials}</div>
														</div>
														<div className={styles.landLordInformation}>
															<div className={styles.landlordName}>
																{tenant.profile.firstName} {tenant.profile.lastName}
															</div>
														</div>
													</div>
													<div className={styles.profileSubtitle}>
														{tenant.address.first}, {tenant.address.second},{' '}
														{tenant.address.city} {tenant.address.state}{' '}
														{tenant.address.postalCode}
														<br />
														<br />
														<div
															className={[ styles.secondaryColor, styles.flexSB ].join(
																' '
															)}
														>
															<strong>Due: </strong>
															{tenant.address.due} <strong>By: </strong>
															{tenant.address.dueBy}
														</div>
														<br />
													</div>
												</div>
												<div className={styles.landlordButtonContainer}>
													<button
														className={styles.secondaryButton}
														onClick={() =>
															window.open(`phone:${tenant.profile.phoneNumber}`)}
													>
														Call
													</button>
													<div style={{ minWidth: 10 }} />
													<button
														className={styles.primaryButton}
														onClick={() => window.open(`mailto:${tenant.email}`, '_blank')}
													>
														Send Email
													</button>
												</div>
											</div>
										);
									})}
								</div> */}
								{/* <div className={styles.landlordButtonContainer} style={{ padding: 20, paddingTop: 0 }}>
									<div style={{ padding: 0 }} className={styles.widgetTitle}>
										DOCUMENTS
									</div>
									<button
										className={styles.primaryButton}
										style={{ margin: 0, width: 100 }}
										onClick={() => {}}
									>
										UPLOAD
									</button>
								</div> */}

								{/* <div className={styles.tenantsContainer}>
									{documentKeys.length == 0 && (
										<div className={styles.tenantContainer}>
											<div className={styles.tenantBox}>
												<div className={styles.landLordInformation}>
													<div
														className={styles.landlordName}
														onClick={() => {
															this.setState({ inviteModalOpen: true });
														}}
													>
														MANAGE DOCUMENTS
													</div>
													<div className={styles.profileSubtitle}>
														Upload important documents relating to your tenant and
														properties
													</div>
												</div>
											</div>
											<div className={styles.landlordButtonContainer}>
												<button
													className={styles.primaryButton}
													onClick={() => {
														this.setState({ inviteModalOpen: true });
													}}
												>
													UPLOAD
												</button>
											</div>
										</div>
									)}
									{documentKeys.map((documentKey) => {
										const tenant = documents[documentKey];
										const tenantInitials = `${tenant.profile.firstName &&
										tenant.profile.firstName != ''
											? tenant.profile.firstName.toUpperCase().charAt(0)
											: ''}${tenant.profile.lastName && tenant.profile.lastName != ''
											? tenant.profile.lastName.toUpperCase().charAt(0)
											: ''}`;
										return (
											<div className={styles.tenantContainer}>
												<div>
													<div className={styles.tenantBox}>
														<div className={styles.profileImage}>
															<div className={styles.initials}>{tenantInitials}</div>
														</div>
														<div className={styles.landLordInformation}>
															<div className={styles.landlordName}>
																{tenant.profile.firstName} {tenant.profile.lastName}
															</div>
														</div>
													</div>
													<div className={styles.profileSubtitle}>
														{tenant.address.first}, {tenant.address.second},{' '}
														{tenant.address.city} {tenant.address.state}{' '}
														{tenant.address.postalCode}
														<br />
														<br />
														<div
															className={[ styles.secondaryColor, styles.flexSB ].join(
																' '
															)}
														>
															<strong>Due: </strong>
															{tenant.address.due} <strong>By: </strong>
															{tenant.address.dueBy}
														</div>
														<br />
													</div>
												</div>
												<div className={styles.landlordButtonContainer}>
													<button
														className={styles.secondaryButton}
														onClick={() =>
															window.open(`phone:${tenant.profile.phoneNumber}`)}
													>
														Call
													</button>
													<div style={{ minWidth: 10 }} />
													<button
														className={styles.primaryButton}
														onClick={() => window.open(`mailto:${tenant.email}`, '_blank')}
													>
														Send Email
													</button>
												</div>
											</div>
										);
									})}
								</div> */}
							</div>
						</div>
					</div>
				</div>

				{paymentRequestModalOpen && (
					<PaymentRequestModal
						properties={this.state.properties}
						publicKey={this.props.publicKey}
						wallet={this.props.wallet}
						tenants={tenants}
						close={() => this.setState({ paymentRequestModalOpen: false })}
					/>
				)}
				{profileModalOpen && (
					<ProfileModal
						wallet={this.props.wallet}
						profile={this.props.profile}
						email={this.props.email}
						close={() => this.setState({ profileModalOpen: false })}
					/>
				)}
				{inviteModalOpen && (
					<InviteModal
						publicKey={this.props.publicKey}
						properties={this.state.properties}
						profile={this.props.profile}
						email={this.props.email}
						close={() => this.setState({ inviteModalOpen: false })}
					/>
				)}
				{addPropertyModalOpen && (
					<AddPropertyModal
						properties={this.state.properties}
						profile={this.props.profile}
						email={this.props.email}
						close={() => {
							this.setState({ addPropertyModalOpen: false });
						}}
					/>
				)}
				{propertyDetailsOpen && (
					<PropertyDetailsModal
						property={focusedProperty}
						close={() => {
							this.setState({ propertyDetailsOpen: false });
						}}
					/>
				)}
			</div>
		);
	}
}

export default Landlord;
