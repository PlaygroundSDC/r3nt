import React, { Component } from 'react';
import styles from './auth.module.css';
import { user, db } from '../../API/user';
import Loader from '../Components/loader';

function parseJwt(token) {
	var base64Url = token.split('.')[1];
	var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
	var jsonPayload = decodeURIComponent(
		atob(base64)
			.split('')
			.map(function(c) {
				return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
			})
			.join('')
	);

	return JSON.parse(jsonPayload);
}

var getUrlParams = function(name) {
	var params = {};
	window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) {
		params[key] = value;
	});

	return params[name] || params;
};

function setCookie(cname, cvalue, exdays) {
	const d = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	let expires = 'expires=' + d.toUTCString();
	document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}
function getCookie(cname) {
	let name = cname + '=';
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}

class Invite extends Component {
	constructor(props) {
		super(props);
		let landlord = '';
		let address = '';
		this.state = {
			landlordPubKey: '',
			landlord: landlord,
			property: address,
			propertyPubKey: '',
			accepting: false,
			invite: {},
			ready: false
		};
	}

	componentDidMount() {
		const jwt = getUrlParams('invite');
		const invite = getCookie('invite');
		if (Object.keys(jwt).length > 0) {
			//parse and save jwt and concatenate vars
			setCookie('invite', jwt);
			localStorage.setItem('inviteToken', jwt);
			const invitePayload = parseJwt(jwt);
			if (invitePayload) {
				this.setState({ landlord: { firstName: invitePayload.landLord.name } });
				this.getProperty(invitePayload.landLord.email, invitePayload.address);
				this.saveInviteToken(invitePayload.inviteToken);
				this.setState({ ready: true });
			}
		} else if (invite) {
			const invitePayload = parseJwt(invite);
			if (invitePayload) {
				this.setState({ landlord: { firstName: invitePayload.landLord.name } });
				this.getProperty(invitePayload.landLord.email, invitePayload.address);
				this.saveInviteToken(invitePayload.inviteToken);
				this.setState({ ready: true });
			}
		}
	}

	saveInviteToken = async (inviteToken) => {
		this.setState({ savingInviteToken: true });
		if (this.props.authenticated) {
			const invite = await db.get(`INVITE_${this.props.email}`).then();
			if (invite) {
				await user.get('invite').put({ key: inviteToken });
				this.setState({
					savingInviteToken: false,
					invite: {
						landLordPublicKey: invite.landLordPublicKey,
						date: invite.date,
						propertyName: invite.propertyName,
						propertyAddress: invite.propertyAddress
					}
				});
			}
		}
	};

	getProperty = (landlordEmail, address) => {
		let landlord = {};
		let property = {};
		db.get(`~@${landlordEmail}`).once(async (data) => {
			let pubKey = Object.keys(data).find((key) => key != '_');
			if (pubKey && pubKey.startsWith('~')) {
				pubKey = pubKey.substring(1);
				let to = db.user(pubKey);
				//check if access granted -- already had an account when accepting invite so access was granted already
				if (to) {
					landlord = {
						firstName: await to.get('profile').get('firstName'),
						lastName: await to.get('profile').get('lastName'),
						phoneNumber: await to.get('profile').get('phoneNumber')
					};
					property = await to.get('rentalProperties').get(address);
					this.setState({
						landlordPubKey: pubKey,
						landlord: landlord,
						property: property,
						propertyPubKey: address
					});
				} else {
					//
				}
				//user.get('rentalProperties').get(property)
			}
		});
		/* .then()
			.catch((err) => console.log(err)); */
	};
	acceptInvite = async () => {
		//set tenant id on user and grant permissions to profile to landlord
		const { landlordPubKey, propertyPubKey } = this.state;
		const lord = db.user(landlordPubKey);
		this.setState({ accepting: true });
		await user.get('landlord').put({ key: landlordPubKey }).then();
		await user.get('property').put({ key: propertyPubKey }).then();
		await user.get('profile').put({ role: 'tenant' }).then();
		await user.get('profile').get('firstName').grant(lord);
		await user.get('profile').get('lastName').grant(lord);
		await db.get(`INVITE_${this.props.email}`).put({ accepted: true, acceptedBy: this.props.publicKey }).then();
		//await user.get(`SHARED_${landlordEmail}`).grant(lord);
		//await user.get('profile').get('phoneNumber').grant(lord);
		document.cookie = 'invite=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
		window.location.replace('/');
	};

	goToAuthentication = () => {
		window.open('/', '_self');
	};

	render() {
		const { landlord, property, accepting, ready, invite } = this.state;
		const { authenticated, profile } = this.props;
		const rentDueDates = property ? property.rentDueDates.split(', ') : '1';
		const paymentCycles = invite.leaseLength || 1;
		return (
			<div className="page">
				<div className={styles.header}>
					<h1 className="logo" style={{ fontSize: 100 }}>
						R3NT
					</h1>
				</div>
				<div className={styles.authCardContainer} style={{}}>
					<h1 style={{ fontSize: 40, color: '#002054', textShadow: '0 0 3px #00ffff' }}>
						You have been invited!
					</h1>
					{authenticated && (
						<div style={{ fontSize: 20, color: 'white' }}>
							<strong style={{ color: '#002054' }}>
								{landlord.firstName} {landlord.lastName}{' '}
							</strong>has requested you as a tenant at
							<strong style={{ color: '#00ffff' }}> {property.name}</strong>, a {property.squareFeet}sqft.{' '}
							{property.type} residing at{' '}
							<b style={{ color: '#002054' }}>
								{property.address1} {property.address2} {property.city}, {property.state}{' '}
								{property.zipCode}.
							</b>
							<br />As a tenant, you, {profile.firstName} {profile.lastName}, will be renting this
							property from {landlord.firstName} {landlord.lastName}, the landlord who manages the
							property.<br />By agreeing to be a tenant you are consenting to being responsible for the
							rental agreement set forth below for this property.
							<br />
							<br />
							<strong style={{ color: '#002054', textShadow: '0 0 3px #00ffff' }}>
								RENTAL AGREEMENT
							</strong>
							<br />
							Rent Cycle is <strong style={{ color: '#002054' }}>{property.rentCycle}</strong>, rent for
							this property is due
							{property.rentCycle == 'monthly' ? (
								` once on the ${rentDueDates} every month`
							) : (
								` the ${rentDueDates} of every month`
							)}{' '}
							for <strong style={{ color: '#002054' }}>{paymentCycles}</strong> payment cycle{paymentCycles != 1 ? 's' : ''}{' '}
							in the amount of <strong style={{ color: '#002054' }}>${property.rentAmount}.</strong>
							<br />
							<br />
							<strong style={{ color: '#002054', textShadow: '0 0 3px #00ffff' }}>
								LANDLORD MESSAGE
							</strong>
							<br />
							{invite.message ||
								'I look forward to sharing my place with you and hope we can have a pleasant relationship during your stay.'}
							<br />
							<br />
						</div>
					)}
					<div className={styles.actionContainer}>
						{authenticated &&
						ready && (
							<button className={styles.primaryButton} onClick={this.acceptInvite}>
								{!accepting ? 'AGREE & ACCEPT INVITE' : <Loader />}
							</button>
						)}
						{!authenticated && (
							<button className={styles.primaryButton} onClick={this.goToAuthentication}>
								AUTHENTICATE TO ACCEPT
							</button>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default Invite;
