import React, { Component } from 'react';
import { db, user } from '../../API/user';
import styles from './auth.module.css';

function getCookie(cname) {
	let name = cname + '=';
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}
class Auth extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSignUp: false,
			pubKey: '',
			firstName: '',
			lastName: '',
			email: '',
			password: '',
			role: 'landlord',
			profile: {}
		};
	}

	componentDidMount() {
		const invite = getCookie('invite');
		if (invite) {
			this.setState({ role: 'tenant' });
		}
	}

	login = () => {
		const { email, password } = this.state;
		user.auth(email, password, ({ err }) => {
			const invite = getCookie('invite');
			if (err) this.setState({ error: err });
			else if (invite) {
				window.location.replace('/Invite');
			} else window.location.replace('/');
		});
	};

	signup = () => {
		const { firstName, lastName, email, password, role } = this.state;
		user.create(email, password, async (res) => {
			//console.log(res.pub)
			if (res.err) {
				this.setState({ error: res.err });
			} else {
				//await this.props.user.get('profile').put({firstName: firstName, lastName: lastName}).then();
				await db
					.user(res.pub)
					.get('profile')
					.put({ firstName: firstName, lastName: lastName, role: role })
					.then();
				this.login();
			}
		});
	};

	updateUser = async () => {
		const { firstName, lastName } = this.state;
		await this.props.user.get('profile').put({ firstName: firstName, lastName: lastName }).then();
	};

	printUser = async () => {
		var to = this.props.db.user(this.state.pubKey);
		var who = await to.get('alias').then();
		var profile = await to.get('profile').then();
	};
	render() {
		const { isSignUp, firstName, lastName, email, password, pubKey, profile, error } = this.state;
		return (
			<div className="page">
				<div className={styles.header}>
					<h1 className="logo" style={{ fontSize: 100 }}>
						R3NT
					</h1>
				</div>
				{isSignUp ? (
					<div className={styles.authCardContainer}>
						<h1>Create Account</h1>
						<p style={{ fontWeight: 'bold', fontSize: 18, marginBottom: 10 }}>
							<span className="lightblue">R3NT</span> is a decentralized application, meaning your data is
							not stored on one single place (server).<br />
							Your data, primarily lives on your device and any other devices you authenticate into or
							share data with.<br />
							Using <span className="lightblue">R3NT</span> means no one person will ever own your data
							and you have control over who has access.<br />
							To make <span className="lightblue">R3NT</span> work with other devices, relay endpoint can
							be created; these endpoints act as a gateway between you and other connected devices running
							<span className="lightblue">R3NT</span>.<br />
							Without this relay, you are still be able to use <span className="lightblue">R3NT</span>,
							but your data will live only with you, in your local browser storage.<br />
							<a href="#">Click here for more info</a>
							<br />
							<br />
							<span className="lightblue">R3NT</span> utilizes the <a href="#">Solana Network</a> to
							facilitate your rent payments for properties you are renting to others privately or
							publicly.<br />Transactions are created through the <a href="#">Solana Pay API</a> which
							enables cryptocurrency wallet transactions on the Solana network.
							<br />
							By using and creating an account with <span className="lightblue">R3NT</span>, you agree to
							take full responsibility over your data, who you share it with, and how it is stored.<br />Transactions
							made on <span className="lightblue">R3NT</span> with Solana pay are responsibility of wallet
							owners connecting to the service.<br />
							Software is provided as is. No warranty or liability given.<br />
							For Demo purposes only. Running on Solana Pay devnet.
							<br />
							<br />
						</p>
						<input
							placeholder="First Name"
							className={styles.authCardInput}
							onChange={(e) => this.setState({ firstName: e.target.value })}
							value={firstName}
						/>
						<input
							placeholder="Last Name"
							className={styles.authCardInput}
							onChange={(e) => this.setState({ lastName: e.target.value })}
							value={lastName}
						/>
						<input
							placeholder="Email"
							className={styles.authCardInput}
							type="email"
							onChange={(e) => this.setState({ email: e.target.value })}
							value={email}
						/>
						<input
							placeholder="Password"
							className={styles.authCardInput}
							type="password"
							onChange={(e) => this.setState({ password: e.target.value })}
							value={password}
						/>
						<p className={styles.error}>{error}</p>

						<div className={styles.actionContainer}>
							<button className={styles.primaryButton} onClick={this.signup}>
								Submit
							</button>
							{/* <button onClick={this.updateUser}>Update</button> */}
							<div>
								Already have an account?{' '}
								<span
									style={{ color: '#00ffff' }}
									onClick={() => this.setState({ isSignUp: false, error: '' })}
								>
									Sign in
								</span>
							</div>
						</div>
					</div>
				) : (
					<div className={styles.authCardContainer}>
						<h1>
							Welcome {profile.firstName}
							<br />
							{pubKey}
						</h1>
						<input
							placeholder="Email"
							className={styles.authCardInput}
							onChange={(e) => this.setState({ email: e.target.value })}
							type="email"
							value={email}
						/>
						<input
							placeholder="Password"
							className={styles.authCardInput}
							onChange={(e) => this.setState({ password: e.target.value })}
							type="password"
							value={password}
						/>
						<p className={styles.error}>{error}</p>
						<div className={styles.actionContainer}>
							<button className={styles.primaryButton} onClick={this.login}>
								Sign in
							</button>
							<button
								className={styles.secondaryButton}
								onClick={() => this.setState({ isSignUp: true, error: '' })}
							>
								Create Account
							</button>
						</div>
						{/* <button onClick={this.printUser}>Print user</button> */}
					</div>
				)}
			</div>
		);
	}
}

export default Auth;

//this.props.user.get('alias');
